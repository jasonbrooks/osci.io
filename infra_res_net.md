---
title: Network
---

| VLANs             | ID  | IP Ranges        | Gateway                         | Purpose                                                |
|:-----------------:|:---:|:----------------:|:-------------------------------:|:-------------------------------------------------------|
| OSCI-Public       | 190 | 8.43.85.192-239  | 8.43.85.254                     | direct Internet access                                 |
|                   |     | 2620:52:3:1::/64 | 2620:52:3:1:ffff:ffff:ffff:fffe |                                                        |
| OSCI-Provisioning | 430 | 172.24.30.1-249  | 172.24.30.254                   | provisioning of new hosts                              |
| OSCI-Management   | 431 | 172.24.31.1-249  | 172.24.31.254                   | equipments administration                              |
| OSCI-Internal     | 432 | 172.24.32.1-249  | 172.24.32.254                   | machines behind a reverse proxy (security, saves IPs…) |
