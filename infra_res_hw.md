---
title: Hardware List
---

## Rackable Equipments

| Name        | Location        | Size | Network Interfaces | Access       | Description                   | Purpose       |
|:-----------:|:---------------:|:----:|:------------------:|:-------------|:------------------------------|:--------------|
| Catatonic   | RDU2 3C-B04 U01 |  6U  | CMM1/CMM2: OSCI-Management<br/>A1/A2: Ex0/60 trunk OSCI-* | CMM master: cmm-catatonic.adm.osci.io<br/>A1: switch-a1-catatonic.adm.osci.io<br/>A2: switch-a2-catatonic.adm.osci.io | Supermicro Microblade MBE-628E-816 with 2 redundant management modules MBM-CMM-001 and two independant network modules MBM-XEM-002 | Fedora Atomic, OSCI hypervisors/backup/monitoring
| Conserve    | RDU2 3C-B02 U21 |  1U  | eth0: OSCI-Management | conserve.adm.osci.io | Digi CM console server | Emergency console access |
| Guido       | RDU2 3C-B02 U19 |  1U  | eth0: OSCI-Public<br/>eth1: trunk OSCI-Provisioning/Management/Internal | guido.adm.osci.io | IBM System x3550 M2, 4x Intel Xeon E5530 2.40GHz, 34GB RAM, LSILOGIC SAS1068E 160GB RAID 1, 600GB soft RAID 1 with 2 spares | OSCI hypervisor |
| Speedy      | RDU2 3C-B02 U20 |  1U  | eth0: OSCI-Public<br/>eth1: trunk OSCI-Provisioning/Management/Internal | speedy.adm.osci.io | IBM System x3550 M2, 4x Intel Xeon E5530 2.40GHz, 34GB RAM, LSILOGIC SAS1068E 160GB RAID 1, 600GB soft RAID 1 with 2 spares    | OSCI hypervisor |
| TempusFugit | RDU2 3C-B02 U38 |  1U  | eth0: OSCI-Internal | tempusfugit.int.osci.io | Tempus LX CDMA | Secure time provider

(the U indicated in the location is the bottom one where the equipment seats)

## Blades

| Name            | Server    | Location    | Network Interfaces | Purpose                   |
|:---------------:|:---------:|:-----------:|:------------------:|:--------------------------|
| Seymour         | Catatonic | A1          | admin: 172.24.31.20<br/>eth0+eth1: OSCI-Provisioning<br/>eth2+eth3: OSCI-Public,OSCI-Management,OSCI-Internal | OSCI Hypervisor |
| Jerry           | Catatonic | A2          | admin: 172.24.31.21<br/>eth0+eth1: OSCI-Provisioning<br/>eth2+eth3: OSCI-Public,OSCI-Management,OSCI-Internal | OSCI Hypervisor |
| Lucille         | Catatonic | A3          | admin: 172.24.31.22<br/>eth0+eth1: OSCI-Services | OSCI Storage |
| fedora-atomic-1 | Catatonic | A5          | admin: 172.24.31.24<br/>eth0+eth1: OSCI-Provisioning<br/>eth2+eth3: OSCI-Public | Fedora Atomic control node |
| fedora-atomic-2 | Catatonic | A6          | admin: 172.24.31.25<br/>eth0+eth1: OSCI-Provisioning<br/>eth2+eth3: OSCI-Public | Fedora Atomic control node |
| fedora-atomic-3 | Catatonic | A7          | admin: 172.24.31.26<br/>eth0+eth1: OSCI-Provisioning<br/>eth2+eth3: OSCI-Public | Fedora Atomic test node |
| fedora-atomic-4 | Catatonic | A8          | admin: 172.24.31.27<br/>eth0+eth1: OSCI-Provisioning<br/>eth2+eth3: OSCI-Public | Fedora Atomic test node |
| fedora-atomic-5 | Catatonic | A9          | admin: 172.24.31.28<br/>eth0+eth1: OSCI-Provisioning<br/>eth2+eth3: OSCI-Public | Fedora Atomic test node |
| fedora-atomic-6 | Catatonic | A10         | admin: 172.24.31.29<br/>eth0+eth1: OSCI-Provisioning<br/>eth2+eth3: OSCI-Public | Fedora Atomic test node |
| Heptapod        | Catatonic | A12         | admin: 172.24.31.31<br/>eth0+eth1: OSCI-Provisioning<br/>eth2+eth3: OSCI-Public | Heptapod test node      |
| (empty)         | Catatonic | A13         | admin: 172.24.31.32 | |
| Master-2        | Catatonic | A14         | admin: 172.24.31.33<br/>eth0+eth1: OSCI-Internal<br/>eth2+eth3: OSCI-Services | oVirt and OpenShift experimentation |
| LB              | Catatonic | B1.1        | admin: 172.24.31.34<br/>eth0+eth1: OSCI-Internal | oVirt and OpenShift experimentation |
| Catton          | Catatonic | B2.1        | admin: 172.24.31.42<br/>eth0+eth1: OSCI-Public,OSCI-Management,OSCI-Internal,OSCI-Services | Supervision |
| Master-0        | Catatonic | B3          | admin: 172.24.31.36<br/>eth0+eth1: OSCI-Internal<br/>eth2+eth3: OSCI-Services | oVirt and OpenShift experimentation |
| Master-1        | Catatonic | B4          | admin: 172.24.31.37<br/>eth0+eth1: OSCI-Internal<br/>eth2+eth3: OSCI-Services | oVirt and OpenShift experimentation |
| Worker-0        | Catatonic | B5          | admin: 172.24.31.38<br/>eth0+eth1: OSCI-Internal<br/>eth2+eth3: OSCI-Services | oVirt and OpenShift experimentation |
| Worker-1        | Catatonic | B6          | admin: 172.24.31.39<br/>eth0+eth1: OSCI-Internal<br/>eth2+eth3: OSCI-Services | oVirt and OpenShift experimentation |
| Worker-2        | Catatonic | B7          | admin: 172.24.31.40<br/>eth0+eth1: OSCI-Internal<br/>eth2+eth3: OSCI-Services | oVirt and OpenShift experimentation |
| (empty)         | Catatonic | B8          | admin: 172.24.31.41 | |

