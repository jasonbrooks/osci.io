---
title: Mailing-lists
---

* TOC
{:toc}

## Mailman 3 Hosting

We're providing mailing-lists hosting using Mailman 3, with migration from Mailman 2 if needed.

The mailing-list instance may be hosted in the Community Cage or any host at the tenant's convenience.

This document records our offering but also our experience around it. We develop [packages and Ansible roles](#resources) to drive these deployments and it may be useful to others.

## Noteworthy changes from Mailman 2

### Authentication

Previously subscriptions came with a password, one per list, and aside from a hack to help change settings globaly it was not very practical to manage settings. The password was also sent (on most lists) insecurely in a regular reminder notification.

In Mailman 3 there are real user accounts. Once authenticated using a local or social (Google, GitHub…) account the use can see and change its subscriptions, profile information, list preferences… Instead of local and social accounts it is possible to use OpenID Connect (OIDC), but not both.

It is not compulsory to create an account though, one can be subscribed without never login in, but there is no way to customize settings.

This account is also practical for moderators and list admins, to filter subscriptions and posts as well as define lists settings.

### Moderation

Previously you could use a moderator or list admin password using the `Approved:` pseudo-header, but this was utterly unsafe and this is no longer possible, please use the web UI for these tasks.

### Archives

Prevously the post URLs were generated in a non-unique manner, meaning that the URL was not stable and could change if you rescanned the archives or exported and reimported. This affects the migration because there is no way to map old URLs to the new ones.

As the archives path in the URL is different in Mailman 3 we decided to keep the old generated pages available to avoid breaking the links returned by search engines. The posts are of course also available in the new UI with all the new features, this is just a static view. Aside from taking extra storage on the server there is no downside.

## Requirements

If a migration from Mailman 2 is needed, then access to the Mailman 2 files is necessary. The original mailboxes (mbox files) are used to import the posts for each list. The settings files (pck files) are used to get the subscribers and list settings. The old archives' web pages are used to keep all the links on the Internet working (see previous chapter).

Information about the number of lists, expected volume, and domains involved are necessary to size the new instance. In case of migration from Mailman 2 we can find most of this out by looking at the current archives. It is possible to have multiple domains managed on the same instance, but if both domains are planned to be separated (separate projects) it would be better to setup separate instances from the start; exporting and reimporting is a delicate matter.

Aside from local accounts, authentication with social accounts is possible, so we need to get tokens from each provider to setup the instance. We highly recommand to setup a team account on each provider instead of using one from a team member: users will see the name of the person instead of the project name when registering, and things are going to be complicated when the person leaves the project. Have a look at [the procedure to create tokens](/offers/mailing-lists_tokens/). If you chose to use OIDC instead then we'll need to create a profile with the OIDC provider.

## Customization

### Mail Templates

Posts footer, subscription messages and various notifications messages can be customized. Messages can be global or per list and using various languages. [Mailman's default messages](https://gitlab.com/mailman/mailman/tree/3.2.2/src/mailman/templates/en) can be used as examples to draft messages adapted to the community ([upstream technical documentation](https://mailman.readthedocs.io/en/latest/src/mailman/rest/docs/templates.html)). Then we can easily deploy these customization using Ansible.

### Web UI

Lists short and long descriptions can be setup in the administrative UI but it is very limited.
To customize further certain parts of the UI can be overriden.

Currently we have experience customizing:

* the top bar
* add project branding (logo) as well as favicon
* add content at the very top and bottom of the page
* headers, which can be used to load a custom CSS

## Resources

Documentation:

* [Upstream Documentation](https://mailman.readthedocs.io/)
* [OSCI Technical Documentation](/infra_procedures/mailing-lists/)

