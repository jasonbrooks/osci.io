---
title: Zanata (translate.zanata.org)
---

## Introduction

Since we are in charge of `translate.zanata.org`'s maintenance but we lack experience with this service, this page will collect notes as we have to handle new situations.

## Deployment

Zanata is fully deployed using Ansible. The `jbosseap7` and `zanata` Ansible roles have been opensourced and we're now in charge of their maintenance. It requires a registered RHEL 7 to get EAP7 packaging at the right version. The zanata Package is available in our Copr repository and we also are in charge of the maintenance.

## Admin Tool

The admin UI does not give a complete view of the user and their associated projects, thus we have created [a small script](https://gitlab.com/osas/zanata-admin) to help us. It is already available on the root account's home and can be used on the cpmmand-line like this:
```
cd zanata-admin
source ./bin/activate
./zanata-admin --help
```
## Frequent Topics

### User Deletion

In the Admin UI, you can search for a specific user in the `Manage Users` page. Before cliking on the username the chevron on the left side opens a menu where you can fin the delete action.

If the user has made translations or is linked to an active project then [it may not be possible to delete it](https://zanata.atlassian.net/browse/ZNTA-1908). Instead you can use the `Erase user's personal data` action.

### Finding a Specific Project

It is not easy to find a specific project by its name and their may be conflicts. The unique path to a project is called the project `slug` and can be used to access the project info page:

    https://translate.zanata.org/project/view/<slug>

If you look at a user's details using the Admin Tool then you can get the slug of their projects.

