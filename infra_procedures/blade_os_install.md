---
title: Blade OS Installation
---

## Accessing the Blade

To power the blade and process through the installation procedure, please familiarize yourself with [the blades' administrative interfaces](/infra_procedures/blade_admin_access/) first.

## OS Installation

### OS Selection

You may have to reboot and select the right menu to choose PXE. One of the interfaces of the blade should be in the `OSCI-Provisioning` VLAN and should catch DHCP and propose an installation menu. You should have access to manual and basic automated installation for classic OSes, and maybe tenant/topic-specific automated installations if there are.

If no DHCP is caught, then most probably the switches needs to be reconfigured to change the network assignation. Also beware not all blade interfaces are able to boot on PXE.

### Finding Provisioning IP

You can find the provisioning IP using a special playbook:

```shell
 ansible-playbook --diff -l <bladename> playbooks/find_prov_ip.yml
```

### Red Hat Systems

On Red Hat OSes the graphical installation is accessible via VNC.

After installing a VNC client, you need to redirect port `5001` to the provisioning IP of the blade:

```shell
ssh -L 5901:<blade-prov-ip>:5901 root@speedy.osci.io
```

And finally access the graphical installation:

```shell
vncviewer localhost:5901
```

The VNC password is available on the OSAS Vault.

### Debian Systems

On Debian systems you can continue on the console redirection, activate another TTY and use `screen` to control the text mode installation.
