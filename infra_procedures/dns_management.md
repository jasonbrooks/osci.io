---
title: DNS Management
---

## Introduction

DNS master server, Polly, syncronize all master zones to Francine and Carla using [TSIG](https://en.wikipedia.org/wiki/TSIG).

Reverse IPs are defined in Red Hat infrastructure in the `dnsmaps` system.

## Deploying Master Zones

Master zones files are in `data/tenants/osci/dns/zones/` in the Ansible repository, and can be deployed with a playbook:

```shell
 ansible-playbook --diff -t dns_data playbooks/tenants/osci/dns.yml
```

## Setting up Reverse IPs

These IPs are in `dnsmaps`, IPv4 in `prod/external-default/8.43.85` and IPv6 in `prod/external-default/3.0.0.0.2.5.0.0.0.2.6.2.ip6.arpa`.

To compute the IPv6 nibbles you can use the `ipv6calc` tool, for example:
```shell
 ipv6calc -q --out revnibbles.arpa 2620:52:3:1:5054:ff:fe64:ab5a
 a.5.b.a.4.6.e.f.f.f.0.0.4.5.0.5.1.0.0.0.3.0.0.0.2.5.0.0.0.2.6.2.ip6.arpa.
```

Remove the IPv6 network prefix defined in the ORIGIN to get the entry.

